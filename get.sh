#!/usr/bin/env bash
#set -x

echo "Installing the Kevin ... ! 😷"

TARGET=~/.local/share/kevin

mkdir -p $TARGET
if [ -f "$TARGET/Makefile" ]; then
  make --directory $TARGET update
else
  git clone git@bitbucket.org:phpro/phpro-kevin.git "$TARGET"
  make --directory $TARGET install
fi

echo "Installation complete 🤴"
