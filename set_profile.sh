#!/usr/bin/env bash
#set -x

case ${SHELL} in
    *zsh*)
        if [[ -f ~/.zshrc ]]; then
            SHELL_FILE="${HOME}/.zshrc"
        else
            SHELL_FILE="${HOME}/.zprofile"
        fi
        ;;
    *bash*)
        if [[ -f ~/.bash_profile ]]; then
            SHELL_FILE="${HOME}/.bash_profile"
        else
            SHELL_FILE="${HOME}/.bashrc"
        fi
        ;;
esac

SOURCE="source \"$(dirname $(realpath "$0"))/.kevin_profile\""
PATH_EXIST=$(grep "${SOURCE}" "${SHELL_FILE}")
if [[ -z ${PATH_EXIST} ]]; then
    echo "${SOURCE}" >> "${SHELL_FILE}"; source "${SHELL_FILE}"
fi
