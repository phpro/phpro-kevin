#!/usr/bin/env bash
#set -x

if [[ $(id -u) -eq 0 ]]; then
  echo "Don't run this script as root!"
  exit 1
fi
KEVIN_PATH=$(readlink -n "$0")
if [[ "${KEVIN_PATH}" == '' ]]; then
  KEVIN_PATH=$(cd "$(dirname "$0")" && pwd)/kevin
fi
INSTALLATION_PATH="$(dirname "${KEVIN_PATH}")/.."

MAGENTO=$(grep -i magento composer.json 2>/dev/null)
if [[ ${MAGENTO} ]]; then
  MAGENTO=true
fi

DOCKER_CMD="docker-compose"
XMUTAGEN=$(grep -i "x-mutagen" docker-compose.override.yml 2>/dev/null)
if [[ ${XMUTAGEN} ]]; then
  DOCKER_CMD="mutagen-compose"
fi


USETTY=""
# Check if stdin is attached a pipe or a redirect
if [[ -p /dev/stdin ]] || [[ ! -t 0 && ! -p /dev/stdin ]]; then
  USETTY="-T"
fi
# Check if stdout is attached a pipe or a redirect
if [[ -p /dev/stdout ]] || [[ ! -t 1 && ! -p /dev/stdout ]]; then
  USETTY="-T"
fi

UNAME="$(uname -s)"
IS_MAC=false
if [[ $UNAME == Darwin* ]]; then
  IS_MAC=true
fi

usage () {
  echo "Please use one of the following"
  echo "------------------------------"
  echo "🤴 Kevin:"
  echo "kevin parallel <tasks> (runs multiple kevin tasks in parallel)"
  echo "kevin self-update (updates kevin)"
  echo ""
  echo "🌴 Environment:"
  echo "kevin destroy (remove containers and mutagen)"
  echo "kevin down (stop containers and pause mutagen sync)"
  echo "kevin exec (run docker compose exec on container)"
  echo "kevin init (setup docker-compose files for mac)"
  echo "kevin logs (start container tail logging)"
  echo "kevin ps (docker-compose ps)"
  echo "kevin pull <optional container name> (docker-compose pull)"
  echo "kevin recreate <container> (force recreation of service)"
  echo "kevin restart <optional container name> (docker-compose restart)"
  echo "kevin ssh <container> (runs bash on a container - defaults to application)"
  echo "kevin ssh-root <container> (runs bash on a container with root user for newer images - defaults to application)"
  echo "kevin status (docker-compose ps & mutagen status)"
  echo "kevin up [-d] [arguments] (start up containers, mutagen and logs - unless you run it as deamon [-d])"
  echo ""
  echo "🐳 Container:"
  echo "kevin app <arguments> (run commands inside php-fpm container)"
  echo "kevin mysql <arguments> (connect to mysql database)"
  echo "kevin mysqldump > DB.sql (dump mysql database)"
  echo "kevin mongo <arguments> (connect to mongodb database)"
  echo "kevin mongodump <arguments> > db.archive (dump mongodb database)"
  echo "kevin mongodump --gzip <arguments> > db.archive.gz (dump compressed mongodb database)"
  echo "kevin node <arguments> (run node commands on node container)"
  echo "kevin psql <arguments> (connect to postgress database)"
  echo "kevin sup|supervisor <argument> (run supervisor commands on php-fpm container)"
  echo ""
  echo "📦 Frameworks:"
  echo "kevin art <arguments> (run php artisan on php-fpm container)"
  echo "kevin console <arguments> (run php ./bin/console on php-fpm container)"
  echo "kevin flush (php-fpm|redis <flushall|-n \$DB_NUMBER flushdb>|varnish)"
  echo "kevin sw <arguments> (run commands on php-fpm-nginx container"
  echo "kevin sw watch:(administration|storefront) (run watcher on php-fpm-nginx container)"
  echo ""
  echo "🔧 Tools:"
  echo "kevin composer <arguments> (run composer v2 on php-fpm container)"
  echo "kevin composer1 <arguments> (run composer v1 on php-fpm container)"
  echo "kevin cs-fixer <arguments> (run ./vendor/bin/php-cs-fixer fix on php-fpm container)"
  echo "kevin dredd <arguments> (runs dredd API testing)"
  echo "kevin grumphp <arguments> (run ./vendor/bin/grumphp run on php-fpm container)"
  echo "kevin monitor <mutagen-name> (run monitor on certain name)"
  echo "kevin npm <arguments> (run npm on node container)"
  echo "kevin phpunit <arguments> (run ./vendor/bin/phpunit on php-fpm  container)"
  echo "kevin paratest <arguments> (run ./vendor/bin/paratest on php-fpm  container)"
  echo "kevin psalm <arguments> (run ./vendor/bin/psalm on php-fpm container)"
  echo "kevin redis-cli <command> (run a redis-cli command)"
  echo "kevin yarn <arguments> (run yarn on node container)"
  echo "------------------------------"
}

# shellcheck disable=SC1091
if [[ -f .docker.env ]]; then source .docker.env; fi

# Check if a given container is running.
# If it is: Run the command on that container
# Otherwise: Run it on a new container (might be slower on mutagen compose)
run () {
  CONTAINER="$1"
  if [ -z  $(docker-compose ps --filter status=running -q "${CONTAINER}") ]; then
    $DOCKER_CMD run $USETTY --rm "$@"
  else
    $DOCKER_CMD exec $USETTY "$@"
  fi
}

ssh() {
    [ ${MAGENTO} ] && APP_NAME="deploy"
    APP_NAME="${APP_NAME:-application}"
    run "${APP_NAME}" bash
}

app () {
  [ ${MAGENTO} ] && APP_NAME="deploy"
  APP_NAME="${APP_NAME:-application}"
  run "${APP_NAME}" "$@"
}

node () {
  NODE_NAME="${NODE_NAME:-node}"
  run "${NODE_NAME}" "$@"
}

sw () {
  EXEC=$(echo "$@" | cut -d\: -f1)
  case "${EXEC}" in
    watch)
      WATCH_NAME=${WATCH_NAME:-application}
      WATCHER=$(echo "$@" | cut -d\: -f2)
      case "${WATCHER}" in
        storefront)
          $DOCKER_CMD exec -T "${WATCH_NAME}" pkill -9 -f "bash /app/bin/watch-storefront.sh"
          $DOCKER_CMD exec -T "${WATCH_NAME}" "/app/bin/watch-storefront.sh"
          ;;
        administration)
          $DOCKER_CMD exec -T "${WATCH_NAME}" pkill -9 -f "bash /app/bin/watch-storefront.sh"
          $DOCKER_CMD exec -T "${WATCH_NAME}" pkill -9 -f "bash /app/bin/watch-administration.sh"
          $DOCKER_CMD exec -T "${WATCH_NAME}" "/app/bin/watch-administration.sh"
          ;;
        *) echo -e "Please usage sw watch:(administration|storefront)\n"
      esac
      ;;
    *)
      APP_NAME="${APP_NAME:-application}"
      $DOCKER_CMD exec $USETTY "${APP_NAME}" "$@"
      ;;
  esac
}
flush () {
  EXEC=$(echo "$@" | cut -d " " -f1)
  case "${EXEC}" in
    redis)
      REDIS_NAME="${REDIS_NAME:-redis}"
      if [[ $# -eq 1 ]]; then
        $DOCKER_CMD exec -T "${REDIS_NAME}" redis-cli flushall
      else
        $DOCKER_CMD exec -T "${REDIS_NAME}" redis-cli "${@:2}"
      fi
      ;;
    *php*)
      APP_NAME=${APP_NAME:-application}
      $DOCKER_CMD exec -T "${APP_NAME}" pkill -USR2 -o php-fpm
      ;;
    fpm)
      APP_NAME=${APP_NAME:-fpm}
      $DOCKER_CMD exec -T "${APP_NAME}" pkill -USR2 -o php-fpm
      ;;
    varnish)
      VARNISH_NAME="${VARNISH_NAME:-varnish}"
      $DOCKER_CMD exec -T "${VARNISH_NAME}" varnishadm ban req.url '~' '.'
      ;;
    *)
      echo -e "Please usage flush (redis|php-fpm|varnish)\n"
      ;;
  esac
}

case $1 in
  #[autocomplete(self-update)]
  self-update)
    make --no-print-directory --directory "${INSTALLATION_PATH}" update
    ;;
  #[autocomplete(app)]
  app)
    app "${@:2}"
    ;;
  #[autocomplete(db)]
  db)
    DB_APP_NAME="${DB_APP_NAME:-db}"
    DB_USER="${DB_USER:-root}"
    DB_PASSWD="${DB_PASSWD:-toor}"
    DB_PORT="${DB_PORT:-3306}"
    DB_NAME="${DB_NAME:-""}"
    MYSQL_APP_NAME="${MYSQL_APP_NAME:-$DB_APP_NAME}"
    MYSQL_USER="${MYSQL_USER:-$DB_USER}"
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-$DB_PASSWD}"
    MYSQL_PORT="${MYSQL_PORT:-$DB_PORT}"
    MYSQL_DATABASE="${MYSQL_DATABASE:-$DB_NAME}"

    echo "DEPRECATED : Use 'kevin mysql' instead!"
    kevin mysql "${@:2}"
    ;;
  #[autocomplete(mysql)]
  mysql)
    MYSQL_APP_NAME="${MYSQL_APP_NAME:-db}"
    MYSQL_USER="${MYSQL_USER:-root}"
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-toor}"
    MYSQL_PORT="${MYSQL_PORT:-3306}"
    MYSQL_HOST="${MYSQL_HOST:-"localhost"}"
    MYSQL_DATABASE="${MYSQL_DATABASE:-""}"
    $DOCKER_CMD exec $USETTY "${MYSQL_APP_NAME}" mysql -u "${MYSQL_USER}" -p"${MYSQL_PASSWORD}" -P "${MYSQL_PORT}" -h "${MYSQL_HOST:-db}" "${MYSQL_DATABASE}" "${@:2}"
    ;;
  #[autocomplete(mysqldump)]
  mysqldump)
    MYSQL_APP_NAME="${MYSQL_APP_NAME:-db}"
    MYSQL_USER="${MYSQL_USER:-root}"
    MYSQL_PASSWORD="${MYSQL_PASSWORD:-toor}"
    MYSQL_PORT="${MYSQL_PORT:-3306}"
    MYSQL_HOST="${MYSQL_HOST:-"localhost"}"
    MYSQL_DATABASE="${MYSQL_DATABASE:-""}"
    $DOCKER_CMD exec $USETTY "${MYSQL_APP_NAME}" mysqldump -u "${MYSQL_USER}" -p"${MYSQL_PASSWORD}" -P "${MYSQL_PORT}" -h "${MYSQL_HOST:-db}" "${MYSQL_DATABASE}" "${@:2}"
    ;;
  #[autocomplete(psql)]
  psql)
    POSTGRES_APP_NAME=${POSTGRES_APP_NAME:-db}
    POSTGRES_USER=${POSTGRES_USER:-root}
    POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-toor}
    POSTGRES_DB=${POSTGRES_DB:-""}
    $DOCKER_CMD exec --env="PGPASSWORD=$POSTGRES_PASSWORD" $USETTY $POSTGRES_APP_NAME psql $POSTGRES_DB $POSTGRES_USER "${@:2}"
    ;;
  #[autocomplete(mongo)]
  mongo)
    MONGO_APP_NAME=${MONGO_APP_NAME:-mongo}
    MONGO_USER=${MONGO_USER:-root}
    MONGO_PASSWD=${MONGO_PASSWD:-toor}
    MONGO_DB_NAME=${MONGO_DB_NAME:-""}
    $DOCKER_CMD exec $USETTY "${MONGO_APP_NAME}" mongosh -u "${MONGO_USER}" -p "${MONGO_PASSWD}" --authenticationDatabase admin "${MONGO_DB_NAME}" "${@:2}"
    ;;
  #[autocomplete(mongodump)]
  mongodump)
    MONGO_APP_NAME=${MONGO_APP_NAME:-mongo}
    MONGO_USER=${MONGO_USER:-root}
    MONGO_PASSWD=${MONGO_PASSWD:-toor}
    MONGO_DB_NAME=${MONGO_DB_NAME:-""}
    $DOCKER_CMD exec $USETTY "${MONGO_APP_NAME}" mongodump -u "${MONGO_USER}" -p "${MONGO_PASSWD}" -d "${MONGO_DB_NAME}" --authenticationDatabase admin --archive "${@:2}"
    ;;
  #[autocomplete(dredd)]
  dredd)
    # Easy functions for running dredd
    # See: https://confluence.xploregroup.net/display/DOC/Dredd%3A+API+testing+in+docker
    if [ "$#" -ne 1 ]; then
      COMPOSE_PROFILES=dredd $DOCKER_CMD run --rm dredd dredd "${@:2}"
      COMPOSE_PROFILES=dredd $DOCKER_CMD stop dredd dredd-hooks && $DOCKER_CMD rm -f dredd dredd-hooks
    else
      COMPOSE_PROFILES=dredd $DOCKER_CMD run --rm dredd
      COMPOSE_PROFILES=dredd $DOCKER_CMD stop dredd dredd-hooks && $DOCKER_CMD rm -f dredd dredd-hooks
    fi
    ;;
  #[autocomplete(art)]
  art)
    app php artisan "${@:2}"
    ;;
  #[autocomplete(console)]
  console)
    app ./bin/console "${@:2}"
    ;;
  #[autocomplete(exec, :docker-service:, bash sh)]
  exec)
    run "${@:2}"
    ;;
  #[autocomplete(psalm)]
  psalm)
    app ./vendor/bin/psalm "${@:2}"
    ;;
  #[autocomplete(phpunit)]
  phpunit)
    app ./vendor/bin/phpunit "${@:2}"
    ;;
  #[autocomplete(paratest)]
  paratest)
    app ./vendor/bin/paratest "${@:2}"
    ;;
  #[autocomplete(composer)]
  composer)
    app composer "${@:2}"
    ;;
  #[autocomplete(composer1)]
  composer1)
    app composer1 "${@:2}"
    ;;
  #[autocomplete(grumphp)]
  grumphp)
    app ./vendor/bin/grumphp run "${@:2}"
    ;;
  #[autocomplete(cs-fixer)]
  cs-fixer)
    app ./vendor/bin/php-cs-fixer fix "${@:2}"
    ;;
  #[autocomplete(monitor)]
  monitor)
    mutagen monitor --label-selector="${*:2}"
    ;;
  #[autocomplete(node)]
  node)
    node "${@:2}"
    ;;
  #[autocomplete(npm)]
  npm)
    node npm "${@:2}"
    ;;
  #[autocomplete(yarn)]
  yarn)
    node yarn "${@:2}"
    ;;
  #[autocomplete(logs, :docker-service:)]
  logs)
    $DOCKER_CMD logs -f "${@:2}"
    ;;
  #[autocomplete(logs, :docker-service:)]
  recreate)
    if [[ "${#@}" -lt 2 ]]; then
      $DOCKER_CMD up -d --force-recreate
    else
      shift
      $DOCKER_CMD up -d --force-recreate --no-deps "$@"
    fi
    ;;
  #[autocomplete(up)]
  #[autocomplete(start)]
  up|start)
    #Remove 'up' keyword from argument list
    shift
    #put arguments into array
    args="$*"
    #check if any of the arguments matches -d
    if [[ " ${args[@]} " =~ " -d " ]] || [[ " ${args[@]} " =~ " --detach " ]]; then
      # rearrange arguments so -d is first and --remove-orphans second \
      # because we use these always
      delete="--detach"
      delete2="-d"
      args=("${args[@]/$delete}")
      args=("${args[@]/$delete2}")
    else
      # if arguments don't contain "-d" then set loggin to follow
      LOGS=true
    fi
    # shellcheck disable=SC2206
    args=(\-d \-\-remove-orphans ${args[@]})
    # start mutagen if the file exists
    if [[ -f ./mutagen.sh ]]; then
      $DOCKER_CMD up "${args[@]}" \
        && ./mutagen.sh start
    else
      $DOCKER_CMD up "${args[@]}"
    fi
    #follow logs if true
    if [[ "${LOGS}" = true ]]; then
      $DOCKER_CMD logs -f
    fi
    ;;
  #[autocomplete(down)]
  #[autocomplete(stop)]
  down|stop)
    $DOCKER_CMD stop
    if [[ -f ./mutagen.sh ]]; then
      ./mutagen.sh stop >/dev/null 2>&1 || mutagen daemon stop
    fi
    ;;
  #[autocomplete(pull)]
  pull)
    $DOCKER_CMD pull "${@:2}"
    ;;
  #[autocomplete(destroy)]
  destroy)
    echo "This will remove all containers and volumes."
    read -p "Are you sure? (Y/y)" -n 1 -r
    if [[ "${REPLY}" =~ ^[Yy]$ ]]; then
      if [[ -f ./mutagen.sh ]]; then
        [ ${MAGENTO} ] && mutagen sync terminate --label-selector=magento-docker; mutagen sync terminate --label-selector=magento-docker-vendor; mutagen daemon stop
        ! [ $MAGENTO ] && ./mutagen.sh destroy >/dev/null 2>&1
      fi
      if [[ -f ./.initialized ]]; then
        rm ./.initialized
      fi
      $DOCKER_CMD down -v
    fi
    ;;
  #[autocomplete(sw)]
  sw)
    sw "${@:2}"
    ;;
  #[autocomplete(ps)]
  ps)
    $DOCKER_CMD ps "${@:2}"
    ;;
  #[autocomplete(status)]
  status)
    $DOCKER_CMD ps "${@:2}"
    if [[ -f ./mutagen.sh ]]; then
      echo "--------------------------------------------------------------------------------"
      # shellcheck disable=SC2154
      mutagen sync list | sed -ne "/${LABEL_APP%-*}/,/\--/ p" || mutagen sync list | sed -ne "/Identifier/,/\--/ p"
    fi
    ;;

  #[autocomplete(ssh, :docker-service:)]
  ssh)
    ssh
    ;;
  #[autocomplete(ssh-root, :docker-service:)]
  ssh-root)
    $DOCKER_CMD exec $USETTY -u root "${2:-application}" bash
    ;;
  #[autocomplete(supervisor)]
  #[autocomplete(sup)]
  supervisor|sup)
    $DOCKER_CMD exec -T "${APP_NAME_SUP:-application}" supervisorctl "${@:2}"
    ;;
  #[autocomplete(restart, :docker-service:)]
  restart)
    $DOCKER_CMD restart "${@:2}"
    ;;
  #[autocomplete(hereman)]
  hereman)
    cat "${INSTALLATION_PATH}/art/hereman.txt"
    ;;
  #[autocomplete(staysteady)]
  staysteady)
    cat "${INSTALLATION_PATH}/art/staysteady.txt"
    ;;
  #[autocomplete(parallel, :command:, :command:, :command:, :command:, :command:, :command:, :command:, :command:, :command:)]
  parallel)
    tasks2run=""
    jobs2run=""
    for command in "${@:2}"
    do
      tasks2run="${tasks2run}kevin ${command}\n"
      jobs2run="${jobs2run}${0} ${command}\n"
    done

    echo -e "Running tasks:\n${tasks2run}"
    echo -e "${jobs2run}" | parallel --no-run-if-empty
    ;;
  #[autocomplete(flush, redis php-fpm varnish)]
  flush)
    flush "${@:2}"
    ;;
  #[autocomplete(redis-cli)]
  redis-cli)
    REDIS_NAME="${REDIS_NAME:-redis}"
    $DOCKER_CMD exec $USETTY "${REDIS_NAME}" redis-cli "${@:2}"
    ;;
  #[autocomplete(init)]
  init)
    [ ${IS_MAC} = true ] && [ ! -L docker-compose.override.yml ] && [ -f docker-compose.mac.yml ] && \
      ln -sf docker-compose.mac.yml docker-compose.override.yml
    exit 0
    ;;
  #[autocomplete(help)]
  *)
    usage
    exit 1
    ;;
esac
