# define standard colors
RED          := $(shell tput -Txterm setaf 1)
GREEN        := $(shell tput -Txterm setaf 2)
YELLOW       := $(shell tput -Txterm setaf 3)
BLUE         := $(shell tput -Txterm setaf 6)

.PHONY: dep install clean install_homebrew install_mutagen check_path

RESET := $(shell tput -Txterm sgr0)
SHELL := /usr/bin/env bash
UNAME := $(shell uname -s)
PWD  := $(shell pwd)
WHOAMI := $(shell whoami)
MAKEFILE_PATH := $(realpath $(lastword $(MAKEFILE_LIST)))
INSTALLATION_PATH := "$(dir $(MAKEFILE_PATH))"

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  dep        verify and install dependencies"
	@echo "  install    create symlink for your binary to ~/.local/bin/"
	@echo "  clean      remove symlinks for your binary"

dep:
	@echo "Verifying if docker exists"
	@command -v docker >/dev/null && echo -e "$(GREEN)Docker is installed 👊 $(RESET)\n" || make --no-print-directory install_docker
	@echo "Verifying if docker-compose exist"
	@command -v docker-compose >/dev/null && echo -e "$(GREEN)Docker-compose is installed 👊 $(RESET)\n" || make --no-print-directory install_docker-compose
	@echo "Verifying if mutagen is installed"
	@command -v mutagen >/dev/null && command -v mutagen-compose && echo -e "$(GREEN)Mutagen is installed 👊 $(RESET)\n" || make --no-print-directory install_mutagen
	@echo "Verifying if parallel is installed"
	@command -v parallel >/dev/null && echo -e "$(GREEN)Parallel is installed 👊 $(RESET)\n" || make --no-print-directory install_parallel
	@./set_profile.sh 2>/dev/null

verify_homebrew:
ifeq ($(UNAME),Darwin)
	@echo "Verifying if homebrew is installed"
	@command -v brew >/dev/null && echo -e "$(GREEN)Homebrew is installed 👊 $(RESET)\n" || make --no-print-directory install_homebrew
endif

install_homebrew:
ifeq ($(UNAME),Darwin)
	@echo "Installing homebrew"
	curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh | ${SHELL}
	@echo -e "$(GREEN)Homebrew is installed $(RESET)\n"
endif

install_mutagen:
ifeq ($(UNAME),Darwin)
	@echo "Installing mutagen"
	@make --no-print-directory verify_homebrew
	brew install mutagen-io/mutagen/mutagen
	brew install mutagen-io/mutagen/mutagen-compose
	@echo -e "$(GREEN) Mutagen is installed $(RESET)\n"
endif

install_docker:
ifeq ($(UNAME),Darwin)
	@echo "$(RED)Please install docker-desktop for mac: -> https://hub.docker.com/editions/community/docker-ce-desktop-mac/ $(RESET)"
endif

ifeq ($(UNAME),Linux)
	@echo "installing docker"
	sudo apt-get -y update && sudo apt -y upgrade
	sudo apt-get -y install \
		   apt-transport-https \
		   ca-certificates \
		   curl \
		   gnupg-agent \
		   software-properties-common \
		   python3-pip \
		   git \
		   htop \
		   net-tools
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
	echo \
	  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
	  focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

	sudo apt-get update
	sudo apt-get upgrade -y
	sudo groupadd docker || true
	sudo usermod -aG docker $(WHOAMI) || true
	export DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y docker-ce docker-ce-cli containerd.io
endif

install_docker-compose:
ifeq ($(UNAME),Linux)
	sudo apt-get -y install python3-pip && sudo pip3 install docker-compose
endif

install_parallel:
ifeq ($(UNAME),Darwin)
	@echo "Installing parallel"
	@make --no-print-directory verify_homebrew
	brew install parallel
	@echo -e "$(GREEN) Parallel is installed $(RESET)\n"
endif
ifeq ($(UNAME),Linux)
	sudo apt-get update && sudo apt-get -y install parallel
endif

install:
	@make --no-print-directory dep
	@echo "Adding Symlink ~/.local/bin/kevin"
	@mkdir -p ~/.local/bin
	@ln -sf $(PWD)/bin/kevin ~/.local/bin/kevin
	@chmod +x $(PWD)/bin/kevin
	@echo -e "$(GREEN)Symlink kevin created 👨‍🎨 $(RESET)\n"
	@"$(PWD)/bin/kevin" init

clean:
	@echo "Removing symlink ~/.local/bin/kevin"
	@[ -L ~/.local/bin/kevin ] && unlink ~/.local/bin/kevin
	@echo -e "$(RED)Symlink kevin removed ☠️ $(RESET)\n"

update:
	@echo "Updating to latest awesome kevin"
	@git fetch --prune; git merge --ff-only origin origin/master || git rebase --rebase-merges origin/master;
	@make --no-print-directory install
	@echo -e "$(GREEN)Awesome Kevin has been updated! 🤠 $(RESET)\n"

