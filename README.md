# Kevin

Tool that helps you working with the PHPro docker-compose setup!

## Installation
```sh
 . <(curl --fail -sS https://bitbucket.org/phpro/phpro-kevin/raw/master/get.sh)
```

## Run make

make help

```
Please use `make <target>' where <target> is one of
  dep        verify and install dependencies
  install    create symlink for your binary to ~/.local/bin/
  clean      remove symlinks for your binary
```

## Kevin

kevin

```
Please use one of the following
------------------------------
🤴 Kevin:
kevin parallel <tasks> (runs multiple kevin tasks in parallel)
kevin self-update (updates kevin)

🌴 Environment:
kevin destroy (remove containers and mutagen)
kevin down (stop containers and pause mutagen sync)
kevin logs (start container tail logging)
kevin ps (docker-compose ps)
kevin pull <optional container name> (docker-compose pull)
kevin recreate <container> (force recreation of service)
kevin restart <container-name> (docker restart a container)
kevin ssh <container> (runs bash on a container - defaults to application)
kevin ssh-root <container> (runs bash on a container with root user for newer images - defaults to application)
kevin status (docker-compose ps & mutagen status)
kevin up [-d] [arguments] (start up containers, mutagen and logs - unless you run it as daemon [-d])

🐳 Container:
kevin app <arguments> (run commands inside php-fpm container)
kevin mysql <arguments> (connect to mysql database)
kevin mysqldump > DB.sql (dump mysql database)
kevin mongo <arguments> (connect to mongodb database)
kevin mongodump <arguments> > db.archive (dump mongodb database)
kevin mongodump --gzip <arguments> > db.archive.gz (dump compressed mongodb database)
kevin node <arguments> (run node commands on node container)
kevin psql <arguments> (connect to postgress database)
kevin sup|supervisor <argument> (run supervisor commands on php-fpm container)

📦 Frameworks:
kevin art <arguments> (run php artisan on php-fpm container)
kevin console <arguments> (run php ./bin/console on php-fpm container)
kevin flush (php-fpm|redis <flushall|-n $DB_NUMBER flushdb>|varnish)
kevin flush fpm (soley for magento2)
kevin sw <arguments> (run commands on php-fpm-nginx container)
kevin sw watch:(administration|storefront) (run watcher on php-fpm-nginx container)

🔧 Tools:
kevin composer <arguments> (run composer v2 on php-fpm container)
kevin composer1 <arguments> (run composer v1 on php-fpm container)
kevin cs-fixer <arguments> (run ./vendor/bin/php-cs-fixer fix on php-fpm container)

kevin grumphp <arguments> (run ./vendor/bin/grumphp run on php-fpm container)
kevin monitor <mutagen-label> (run monitor on certain label)
kevin npm <arguments> (run npm on node container)
kevin phpunit <arguments> (run ./vendor/bin/phpunit on php-fpm  container)
kevin psalm <arguments> (run ./vendor/bin/psalm on php-fpm container)
kevin redis-cli <command> (run a redis-cli command)
kevin yarn <arguments> (run yarn on node container)
------------------------------
```

